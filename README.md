###AppForm Service: 
Service to submit forms and view forms. An exercise in Spring and Angular 4 in reality. 

#### How to use:
Run `make` to do both build and deploy locally.

Run `make build` to build.

Run `make deploy` to deploy locally.

To access the service, visit `localhost:8080`.
